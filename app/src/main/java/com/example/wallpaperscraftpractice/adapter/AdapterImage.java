package com.example.wallpaperscraftpractice.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.example.wallpaperscraftpractice.R;

public final class AdapterImage extends BaseAdapter {

    private Context mContext;
    private String filter;
    public Integer[] mThumbIds;


    public AdapterImage(Context c, String filter) {
        mContext = c;
        this.filter = filter;
        switch (filter) {
            case "main":
                mThumbIds = mMain;
                break;
            case "popular":
                mThumbIds = mPopular;
                break;
            case "cat":
                mThumbIds = mCat;
                break;
            case "art":
                mThumbIds = mArt;
                break;
            case "architecture":
                mThumbIds = mArchitecture;
                break;
            case "astronomy":
                mThumbIds = mAstronomy;
                break;
            case "oldPhotos":
                mThumbIds = mOldPhotos;
                break;
            case "interior":
                mThumbIds = mInterior;
                break;
            case "fashion":
                mThumbIds = mFashion;
                break;
            case "people":
                mThumbIds = mPeople;
                break;
            case "landscape":
                mThumbIds = mLandscape;
                break;
            case "portrait":
                mThumbIds = mPortrait;
                break;
            case "nude":
                mThumbIds = mNude;
                break;
            case "abstraction":
                mThumbIds = mAbstraction;
                break;
            default:
                break;
        }
    }

    public int getCount() {
        return mThumbIds.length;
    }

    public Object getItem(int position) {
        return mThumbIds[position];
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {

            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(400, 800));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(8, 8, 8, 8);
        } else {
            imageView = (ImageView) convertView;
        }
        imageView.setImageResource(mThumbIds[position]);
        return imageView;
    }


    public Integer[] mCat = new Integer[]{R.drawable.kitty, R.drawable.kitty, R.drawable.kitty,
            R.drawable.kitty, R.drawable.kitty, R.drawable.kitty};
    public Integer[] mMain = new Integer[]{R.drawable.art, R.drawable.kitty, R.drawable.dog,
            R.drawable.interior, R.drawable.ancient, R.drawable.fashion};
    public Integer[] mPopular = new Integer[]{R.drawable.dog, R.drawable.dog, R.drawable.dog,
            R.drawable.dog, R.drawable.dog, R.drawable.dog};
    public Integer[] mArt = new Integer[]{R.drawable.art, R.drawable.art, R.drawable.art,
            R.drawable.art, R.drawable.art, R.drawable.art};
    public Integer[] mArchitecture = new Integer[]{R.drawable.architecture, R.drawable.architecture, R.drawable.architecture,
            R.drawable.architecture};
    public Integer[] mAstronomy = new Integer[]{R.drawable.astronomy, R.drawable.astronomy, R.drawable.astronomy,
            R.drawable.astronomy, R.drawable.astronomy, R.drawable.astronomy};
    public Integer[] mOldPhotos = new Integer[]{R.drawable.ancient, R.drawable.ancient, R.drawable.ancient,
            R.drawable.ancient, R.drawable.ancient, R.drawable.ancient};
    public Integer[] mInterior = new Integer[]{R.drawable.interior, R.drawable.interior, R.drawable.interior,
            R.drawable.interior, R.drawable.interior, R.drawable.interior};
    public Integer[] mFashion = new Integer[]{R.drawable.fashion, R.drawable.fashion, R.drawable.fashion,
            R.drawable.fashion, R.drawable.fashion, R.drawable.fashion};
    public Integer[] mPeople = new Integer[]{R.drawable.people, R.drawable.people, R.drawable.people,
            R.drawable.people, R.drawable.people, R.drawable.people};
    public Integer[] mLandscape = new Integer[]{R.drawable.landscape, R.drawable.landscape, R.drawable.landscape,
            R.drawable.landscape};
    public Integer[] mPortrait = new Integer[]{R.drawable.portrait, R.drawable.portrait, R.drawable.portrait,
            R.drawable.portrait};
    public Integer[] mNude = new Integer[]{R.drawable.nude, R.drawable.nude, R.drawable.nude,
            R.drawable.nude, R.drawable.nude, R.drawable.nude};
    public Integer[] mAbstraction = new Integer[]{R.drawable.abstraction, R.drawable.abstraction, R.drawable.abstraction,
            R.drawable.abstraction, R.drawable.abstraction, R.drawable.abstraction};

}



