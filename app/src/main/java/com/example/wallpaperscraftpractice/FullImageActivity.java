package com.example.wallpaperscraftpractice;


import android.app.Activity;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.wallpaperscraftpractice.adapter.AdapterImage;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.IOException;

public class FullImageActivity extends Activity {
    private int position;

    AdapterImage imageAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullimage);

        // get intent data
        Intent intent = getIntent();
        position = intent.getExtras().getInt("id");

        // Selected image id
        imageAdapter = new AdapterImage(this, intent.getExtras().getString("filter"));

        ImageView imageView = (ImageView) findViewById(R.id.full_image_view);
        imageView.setImageResource(imageAdapter.mThumbIds[position]);
        FloatingActionButton save = findViewById(R.id.save);

    }

    public void onClickSave(View view) {
    }

    public void onClickSet(View view) {
        WallpaperManager wallpaperManager = WallpaperManager
                .getInstance(getApplicationContext());
        try {
            wallpaperManager.setResource(imageAdapter.mThumbIds[position]);
            CharSequence text = "Обои успешно установлены!";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(this, text, duration);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}