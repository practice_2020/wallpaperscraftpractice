package com.example.wallpaperscraftpractice.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.wallpaperscraftpractice.FullImageActivity;
import com.example.wallpaperscraftpractice.R;
import com.example.wallpaperscraftpractice.adapter.AdapterImage;


public class SecondFragment extends Fragment {
    private static final int LAYOUT = R.layout.fragment_main;
    private View view;



    public static SecondFragment getInstance(){
        Bundle args = new Bundle();
        SecondFragment fragment = new SecondFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(LAYOUT, container, false);
        GridView gridview = view.findViewById(R.id.gridview);
        gridview.setAdapter(new AdapterImage(getContext(), "popular"));
        gridview.setOnItemClickListener(gridviewOnItemClickListener);
        return view;

    }
    private GridView.OnItemClickListener gridviewOnItemClickListener = new GridView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View v, int position,
                                long id) {
            // Sending image id to FullScreenActivity
            Intent intent = new Intent(getContext(),
                    FullImageActivity.class);
            // passing array index
            intent.putExtra("id", position);
            intent.putExtra("filter", "popular");
            startActivity(intent);
        }
    };
}
