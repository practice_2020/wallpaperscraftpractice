package com.example.wallpaperscraftpractice.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.wallpaperscraftpractice.fragments.SecondFragment;
import com.example.wallpaperscraftpractice.fragments.FirstFragment;
import com.example.wallpaperscraftpractice.fragments.ThirdFragment;

public class TabsPagerFragmentAdapter extends FragmentPagerAdapter {
    private String[] tabs;

    public TabsPagerFragmentAdapter(FragmentManager fm) {
        super(fm);
        tabs = new String[]{
                "Главная",
                "Популярное",
                "Котики"
        };
    }

    public CharSequence getPageTitle(int position) {
        return tabs[position];
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return FirstFragment.getInstance();
            case 1:
                return SecondFragment.getInstance();
            case 2:
                return ThirdFragment.getInstance();
        }
        return null;
    }

    @Override
    public int getCount() {
        return tabs.length;
    }
}
