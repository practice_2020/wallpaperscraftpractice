package com.example.wallpaperscraftpractice;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.drawerlayout.widget.DrawerLayout;


import com.example.wallpaperscraftpractice.adapter.AdapterImage;

import com.google.android.material.navigation.NavigationView;

public class FilterActivity extends Activity {
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;

    private static final int LAYOUT = R.layout.activity_filter;
    private String filter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Bundle arguments = getIntent().getExtras();
        filter = arguments.get("filter").toString();
        setTheme(R.style.AppDefault);
        super.onCreate(savedInstanceState);
        setContentView(LAYOUT);
        initToolBar();
        initNavigationView();
        GridView gridView = findViewById(R.id.gridViewFilter);
        gridView.setAdapter(new AdapterImage(this,filter));
        gridView.setOnItemClickListener(gridviewOnItemClickListener);
    }

    private GridView.OnItemClickListener gridviewOnItemClickListener = new GridView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View v, int position,
                                long id) {

            // Sending image id to FullScreenActivity
            Intent i = new Intent(getApplicationContext(),
                    FullImageActivity.class);
            // passing array index
            i.putExtra("id", position);
            i.putExtra("filter", filter);
            startActivity(i);
        }
    };
    private void initToolBar() {
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });
        toolbar.inflateMenu(R.menu.menu);
    }

    private void initNavigationView() {
        drawerLayout = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.view_navigation_open, R.string.view_navigation_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.navigation);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                drawerLayout.closeDrawers();
                switch (menuItem.getItemId()) {
                    case R.id.mainItem: {
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                    }
                    break;
                    case R.id.artItem:
                        if (filter != "art") {
                            Intent intent = new Intent(getApplicationContext(), FilterActivity.class);
                            filter = "art";
                            intent.putExtra("filter", filter);
                            startActivity(intent);
                        }
                        break;

                    case R.id.architectureItem:
                        if (filter != "architecture") {
                            Intent intent = new Intent(getApplicationContext(), FilterActivity.class);
                            filter = "architecture";
                            intent.putExtra("filter", filter);
                            startActivity(intent);
                        } else break;
                        break;

                    case R.id.astronomyItem:
                        if (filter != "astronomy") {
                            Intent intent = new Intent(getApplicationContext(), FilterActivity.class);
                            filter = "astronomy";
                            intent.putExtra("filter", filter);
                            startActivity(intent);
                        } else break;
                        break;
                    case R.id.oldPhotosItem:
                        if (filter != "oldPhotos") {
                            Intent intent = new Intent(getApplicationContext(), FilterActivity.class);
                            filter = "oldPhotos";
                            intent.putExtra("filter", filter);
                            startActivity(intent);
                        } else break;
                        break;

                    case R.id.interiorItem:
                        if (filter != "interior") {
                            Intent intent = new Intent(getApplicationContext(), FilterActivity.class);
                            filter = "interior";
                            intent.putExtra("filter", filter);
                            startActivity(intent);
                        }
                        break;

                    case R.id.fashionItem:
                        if (filter != "fashion") {
                            Intent intent = new Intent(getApplicationContext(), FilterActivity.class);
                            filter = "fashion";
                            intent.putExtra("filter", filter);
                            startActivity(intent);
                        }
                        break;
                    case R.id.peopleItem:
                        if (filter != "people") {
                            Intent intent = new Intent(getApplicationContext(), FilterActivity.class);
                            filter = "people";
                            intent.putExtra("filter", filter);
                            startActivity(intent);
                        }
                        break;


                    case R.id.landscapeItem:
                        if (filter != "landscape") {
                            Intent intent = new Intent(getApplicationContext(), FilterActivity.class);
                            filter = "landscape";
                            intent.putExtra("filter", filter);
                            startActivity(intent);
                        }
                        break;

                    case R.id.portraitItem:
                        if (filter != "portrait") {
                            Intent intent = new Intent(getApplicationContext(), FilterActivity.class);
                            filter = "portrait";
                            intent.putExtra("filter", filter);
                            startActivity(intent);
                        }
                        break;

                    case R.id.nudeItem:
                        if (filter != "nude") {
                            Intent intent = new Intent(getApplicationContext(), FilterActivity.class);
                            filter = "nude";
                            intent.putExtra("filter", filter);
                            startActivity(intent);
                        }
                        break;

                    case R.id.abstractionItem:
                        if (filter != "abstraction") {
                            Intent intent = new Intent(getApplicationContext(), FilterActivity.class);
                            filter = "abstraction";
                            intent.putExtra("filter", filter);
                            startActivity(intent);
                        }
                        break;

                    default:
                        break;
                }

                return true;
            }
        });
    }

}
