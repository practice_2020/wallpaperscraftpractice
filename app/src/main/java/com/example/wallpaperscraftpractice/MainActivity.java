package com.example.wallpaperscraftpractice;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.GridView;
import android.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;


import com.example.wallpaperscraftpractice.adapter.TabsPagerFragmentAdapter;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private DrawerLayout drawerLayout;

    private static final int LAYOUT = R.layout.activity_main;
    private ViewPager viewPager;

    private String resolution;
    private String filter;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppDefault);
        super.onCreate(savedInstanceState);
        setContentView(LAYOUT);
        initToolBar();
        initNavigationView();
        initTabs();

    }

    private void initToolBar() {
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });
        toolbar.inflateMenu(R.menu.menu);
    }

    private void initNavigationView() {
        drawerLayout = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.view_navigation_open, R.string.view_navigation_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.navigation);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                drawerLayout.closeDrawers();
                switch (menuItem.getItemId()) {
                    case R.id.mainItem: {
                        break;
                    }
                    case R.id.artItem: {
                        Intent intent = new Intent(getApplicationContext(), FilterActivity.class);
                        filter = "art";

                        intent.putExtra("filter", filter);
                        startActivity(intent);
                    }
                    break;

                    case R.id.architectureItem: {
                        Intent intent = new Intent(getApplicationContext(), FilterActivity.class);
                        filter = "architecture";
                        intent.putExtra("filter", filter);
                        startActivity(intent);
                    }
                    break;

                    case R.id.astronomyItem: {
                        Intent intent = new Intent(getApplicationContext(), FilterActivity.class);
                        filter = "astronomy";
                        intent.putExtra("filter", filter);
                        startActivity(intent);
                    }
                    break;
                    case R.id.oldPhotosItem: {
                        Intent intent = new Intent(getApplicationContext(), FilterActivity.class);
                        filter = "oldPhotos";
                        intent.putExtra("filter", filter);
                        startActivity(intent);
                    }
                    break;
                    case R.id.interiorItem: {
                        Intent intent = new Intent(getApplicationContext(), FilterActivity.class);
                        filter = "interior";
                        intent.putExtra("filter", filter);
                        startActivity(intent);
                    }
                    break;
                    case R.id.fashionItem: {
                        Intent intent = new Intent(getApplicationContext(), FilterActivity.class);
                        filter = "fashion";
                        intent.putExtra("filter", filter);
                        startActivity(intent);
                    }
                    break;
                    case R.id.peopleItem: {
                        Intent intent = new Intent(getApplicationContext(), FilterActivity.class);
                        filter = "people";
                        intent.putExtra("filter", filter);
                        startActivity(intent);
                    }
                    break;
                    case R.id.landscapeItem: {
                        Intent intent = new Intent(getApplicationContext(), FilterActivity.class);
                        filter = "landscape";
                        intent.putExtra("filter", filter);
                        startActivity(intent);
                    }
                    break;
                    case R.id.portraitItem: {
                        Intent intent = new Intent(getApplicationContext(), FilterActivity.class);
                        filter = "portrait";
                        intent.putExtra("filter", filter);
                        startActivity(intent);
                    }
                    break;
                    case R.id.nudeItem: {
                        Intent intent = new Intent(getApplicationContext(), FilterActivity.class);
                        filter = "nude";
                        intent.putExtra("filter", filter);
                        startActivity(intent);
                    }
                    break;
                    case R.id.abstractionItem: {
                        Intent intent = new Intent(getApplicationContext(), FilterActivity.class);
                        filter = "abstraction";
                        intent.putExtra("filter", filter);
                        startActivity(intent);
                    }
                    break;
                    default:
                        break;
                }
                return true;
            }
        });
    }

    private void initTabs() {
        viewPager = findViewById(R.id.view_pager);
        TabLayout tabLayout = findViewById(R.id.tab_layout);
        TabsPagerFragmentAdapter adapter = new TabsPagerFragmentAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

}