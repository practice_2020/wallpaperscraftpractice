package com.example.wallpaperscraftpractice;


import android.app.Activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.widget.TextView;

public class InformationActivity extends Activity {

    private String resolutionS;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_information);
        TextView model = findViewById(R.id.model);
        TextView resolution = findViewById(R.id.resolution);
        model.setText(Build.BRAND + " " + Build.MODEL);
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        resolutionS = metrics.widthPixels+" x " + metrics.heightPixels ;
        resolution.setText(resolutionS);


    }

    public void onClick(View view) {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
//        intent.putExtra("resolution", resolutionS);
        startActivity(intent);
    }

}
